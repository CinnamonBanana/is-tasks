# Лабараторная работа №1

## Цель

1. Создать Docker образ
2. Развернуть контейнер Docker из полученного образа и вывести содержимое какого-либо файла
3. Разместить выполненное задание в репозитории на GitLab

## ️Исходные данные

1. Персональный компьютер с установленной операционной системой Windows или Linux
2. Установленные Docker и Git
3. Репозиторий образов Docker: `https://hub.docker.com/`
4. Терминал

## ️Варианты решения задачи

1. Создать `Dockerfile` содержащий инструкцию `CMD` для Docker Container
2. Собрать образ с помощью команды `docker build . -t l1`
3. Запустить контейнер с помощью команды `docker run l1`

## ️Общий план выполнения

- Установить Docker, создать папку для docker проекта
- Создать в папке проекта Dockerfile и вписать в него необходимые инструкции
- Выполнить Commit и Push в репозиторий на GitLab

## Содержание Лабораторной работы

### Шаг 1
Создать общий каталог для заданий и инициализировать новый репозиторий
- `git init`

### Шаг 2
Создать подкаталог L1 для первой Л/Р и перейти в него

### Шаг 3
В текущём каталоге создать файл `Dockerfile`

### Шаг 4
В `Dockerfile` написать строки:
   ```
   FROM debian:latest
   CMD cat /etc/netconfig
   ```

### Шаг 5
Запустить терминал в текущем каталоге

### Шаг 6
Собрать контейнер:
- `docker build . -t l1`

### Шаг 7
После сборки контейнера произвести его запуск: 
- `docker run l1`

### Шаг 8
Подключить удаленный репозиторий
```
git remote rename origin old-origin
git remote add origin https://gitlab.com/CinnamonBanana/is-tasks.git
```

### Шаг 9
Выполнить Commit и Push в удаленный репозиторий GitLab: 
```
add .
git commit -m "Lab1"
git push
```

## ️Оценка результата

Контейнер запустился и вывел содержимое файла `/etc/netconfig`:
```
#
# The network configuration file. This file is currently only used in
# conjunction with the TI-RPC code in the libtirpc library.
#
# Entries consist of:
#
#       <network_id> <semantics> <flags> <protofamily> <protoname> \
#               <device> <nametoaddr_libs>
#
# The <device> and <nametoaddr_libs> fields are always empty in this
# implementation.
#
udp        tpi_clts      v     inet     udp     -       -
tcp        tpi_cots_ord  v     inet     tcp     -       -
udp6       tpi_clts      v     inet6    udp     -       -
tcp6       tpi_cots_ord  v     inet6    tcp     -       -
rawip      tpi_raw       -     inet      -      -       -
local      tpi_cots_ord  -     loopback  -      -       -
unix       tpi_cots_ord  -     loopback  -      -       -
```

## ️Вывод

Изучены основы создания образов и запуска контейнеров в Docker.