with open('dns.log', 'r') as dns:
    dnslist = dns.readlines()
with open('hosts', 'r') as hosts:
    hostslist = [row.strip() for row in hosts]

entries = 0
dns_count = 0
for line in dnslist:
    if not line.startswith("#"):
        dns_count+=1
        if line.split("\t")[8] in hostslist:
            entries += 1

print(entries, "/", dns_count, " - нежелательные хосты в дампе трафика.")
print("Нежелательный трафик составил: ", entries/dns_count*100, "%")
